import { NestFactory } from '@nestjs/core';
import passport = require('passport');
import { AppModule } from './app.module';
import * as expressSession from 'express-session';
async function bootstrap() {
  const app = await NestFactory.create(AppModule);

  app.use(
    expressSession({
      secret: 'nest',
      resave: true,
      saveUninitialized: true,
    }),
  );
  app.use(passport.initialize());
  app.use(passport.session());
  
  await app.listen(3000);
}
bootstrap();
