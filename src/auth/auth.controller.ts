import { Body, Controller, Get, Res, UseGuards } from '@nestjs/common';
import { FacebookLoginGuard } from './facebooklogin.guard';

@Controller('auth')
export class AuthController {
  /* 
    Example Route From Frontend to backend
    ! https://backsidedomain.com/auth/facebook?sid=targetsocketid
  */
  @UseGuards(FacebookLoginGuard)
  @Get('facebook')
  facebookLogin(@Body() Params): Promise<void> {
    return;
  }

  @UseGuards(FacebookLoginGuard)
  @Get('facebook/callback')
  facebookCallback(@Res() res): any {
    // ! I need targetsocketid here for send some thing back to them
  }
}
