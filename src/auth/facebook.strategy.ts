
import { Injectable } from '@nestjs/common';
import { PassportStrategy } from '@nestjs/passport/dist/passport/passport.strategy';
import { Strategy } from 'passport-facebook';
import passport = require('passport');
@Injectable()
export class FacebookStrategy extends PassportStrategy(Strategy, 'facebook') {
  constructor(
  ) {
    super({
      clientID: 'facebook appid',
      clientSecret: 'facebook app secret',
      callbackURL: 'localhost:3000/auth/facebook/callback',
      profileFields: ['id', 'displayName', 'photos', 'email', 'name'],
    });
  }

  // eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
  async validate(accessToken: string, refreshToken: string, profile, done) {
    try {
      /* const ProfileData = {
        provider: 'facebook',
        providerId: profile.id,
        emails: profile.emails,
        displayname: profile.displayName,
        avatar: profile.photos,
        loginuser: profile._json.email,
        last_name: profile._json.last_name,
        first_name: profile._json.first_name,
      }; */

      const user = { };

      done(null, user);
    } catch (err) {
      done(err, false);
    }
    return profile;
  }
}
